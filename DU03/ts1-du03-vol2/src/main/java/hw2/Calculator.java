package hw2;

public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }
    public int subtract(int a, int b) {
        return a - b;
    }
    public int multiply(int a, int b) {
        return a * b;
    }
    public int divide(int a, int b) {
        return a / b;
    }
    public void exceptionThrow() throws Exception {
        throw new Exception("Ty ty ty, dělit nulou...? Kdo to kdy viděl.");
    }

}
