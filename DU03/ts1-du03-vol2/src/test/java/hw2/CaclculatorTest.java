package hw2;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;

public class CaclculatorTest {

    static Calculator calculator;

    @BeforeAll
    public static void initVariable() {
        calculator = new Calculator();
    }

    @Test
    @Order(1)
    public void add_addition_42(){
        int expectedResult = 42;

        int returnedNum = calculator.add(33,9);

        Assertions.assertEquals(expectedResult, returnedNum);
    }

    @Test
    @Order(2)
    public void subtract_subtraction_42(){
        int expectedResult = 42;

        int returnedNum = calculator.subtract(69,27);

        Assertions.assertEquals(expectedResult, returnedNum);
    }

    @Test
    @Order(3)
    public void multiply_multiplication_42(){
        int expectedResult = 42;

        int returnedNum = calculator.multiply(6,7);

        Assertions.assertEquals(expectedResult, returnedNum);
    }

    @Test
    @Order(4)
    public void divide_division_42(){
        int expectedResult = 42;

        int returnedNum = calculator.divide(462,11);

        Assertions.assertEquals(expectedResult, returnedNum);
    }

    @Test
    @Order(5)
    public void divide_0division_exception(){
        Exception exception = Assertions.assertThrows(Exception.class, () -> calculator.exceptionThrow());

        String expectedMessage = "Ty ty ty, dělit nulou...? Kdo to kdy viděl.";
        String actualMessage = exception.getMessage();

        Assertions.assertEquals(expectedMessage, actualMessage);
    }
}
