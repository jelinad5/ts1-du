package hw2;

import org.junit.jupiter.api.*;

public class CalculatorTest {
    static Calculator calculator;
    private final int expectedResult = 42;
    @BeforeAll
    public static void initVariable() {
        calculator = new Calculator();
    }

    @Test
    @Order(1)
    public void add_addition_42(){
        int returnedNum = calculator.add(33,9);

        Assertions.assertEquals(expectedResult, returnedNum);
    }
    @Test
    @Order(2)
    public void subtract_subtraction_42(){
        int returnedNum = calculator.subtract(69,27);

        Assertions.assertEquals(expectedResult, returnedNum);
    }
    @Test
    @Order(3)
    public void multiply_multiplication_42(){
        int returnedNum = calculator.multiply(6,7);

        Assertions.assertEquals(expectedResult, returnedNum);
    }
    @Test
    @Order(4)
    public void divide_division_42() throws Exception {
        int returnedNum = calculator.divide(462,11);
        Assertions.assertEquals(expectedResult, returnedNum);
    }
    @Test
    @Order(5)
    public void divide_0division_exception() throws Exception {
        //ARRANGE
        int num_a = 42;
        int num_b = 0;
        //ACT + (ASSERT)
        Exception exception = Assertions.assertThrows(Exception.class, () -> calculator.divide(num_a, num_b));
        String expectedMessage = "Ty ty ty, dělit nulou...? Kdo to kdy viděl.";
        String actualMessage = exception.getMessage();
        // ASSERT 2
        Assertions.assertEquals(expectedMessage, actualMessage);
    }
}
