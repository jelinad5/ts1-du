import archive.ItemPurchaseArchiveEntry;
import archive.PurchasesArchive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class PurchasesArchiveTest {

//        TODO
//     testy metod  printItemPurchaseStatistics
//                  getHowManyTimesHasBeenItemSold
//                  putOrderToPurchasesArchive
//     println (stream)
//     mock orderArchive
//     mock ItemPurchaseArchiveEntry
//     ověřte správné volání konstruktoru při vytvoření ItemPurchaseArchiveEntry
    private PurchasesArchive purchasesArchive;
    private ArrayList<Item> itemArray;
    private ShoppingCart shoppingCart;
    private Order order;
    private Item item;
    private ByteArrayOutputStream outputStream;

    private int id = 66;
    private String name = "Item66";
    private float price = 42;
    private String category = "Category66";
    private int loyaltyPoints = 9;
    private String customerName = "Řehoř";
    private String customerAddress = "Pospíšilova";

    @BeforeEach
    public void initVariable() {
        item = new StandardItem(id, name, price, category, loyaltyPoints);
        purchasesArchive = new PurchasesArchive();

        itemArray = new ArrayList<>();
        itemArray.add(item);
        shoppingCart = new ShoppingCart(itemArray);

        order = new Order(shoppingCart, customerName, customerAddress);

        outputStream = new ByteArrayOutputStream();
    }

    @Test
    @org.junit.jupiter.api.Order(1)
    public void printItemPurchaseStatistics_itemPurchaseStatisticsOutput_returnExpectedText() {
        String expectedOutput = "ITEM  Item   ID 66   NAME Item66   CATEGORY Category66   PRICE 42.0   LOYALTY POINTS 9   HAS BEEN SOLD 1 TIMES\r\n";
        purchasesArchive.putOrderToPurchasesArchive(order);

        System.setOut(new PrintStream(outputStream));
        purchasesArchive.printItemPurchaseStatistics();

        Assertions.assertEquals("ITEM PURCHASE STATISTICS:\r\n" + expectedOutput, outputStream.toString());
    }

    @Test
    @org.junit.jupiter.api.Order(2)
    public void getHowManyTimesItemHasBeenSold_count_returnsExpectedCount(){
        PurchasesArchive mockedPurchaseArchive = mock(PurchasesArchive.class);
        purchasesArchive.putOrderToPurchasesArchive(order);
        when(mockedPurchaseArchive.getHowManyTimesHasBeenItemSold(item)).thenReturn(1);

        int actualCount = purchasesArchive.getHowManyTimesHasBeenItemSold(item);

        Assertions.assertEquals(actualCount, mockedPurchaseArchive.getHowManyTimesHasBeenItemSold(item));
        verify(mockedPurchaseArchive,times(1)).getHowManyTimesHasBeenItemSold(any(Item.class));
    }

    @Test
    @org.junit.jupiter.api.Order(3)
    public void putOrderToPurchasesArchive_addsEntryToArchive_passed(){
        ItemPurchaseArchiveEntry mockedItemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        when(mockedItemPurchaseArchiveEntry.getRefItem()).thenReturn(item);
        when(mockedItemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold()).thenReturn(1);

        purchasesArchive.putOrderToPurchasesArchive(order);

        Assertions.assertEquals(purchasesArchive.getHowManyTimesHasBeenItemSold(mockedItemPurchaseArchiveEntry.getRefItem()),
                mockedItemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());
        verify(mockedItemPurchaseArchiveEntry,times(1)).getRefItem();
        verify(mockedItemPurchaseArchiveEntry,times(1)).getCountHowManyTimesHasBeenSold();
    }
}
