import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import shop.EShopController;
import shop.Item;
import shop.ShoppingCart;
import shop.StandardItem;
import storage.NoItemInStorage;

import static org.junit.jupiter.api.Assertions.*;

class EShopControllerTest{
    private ShoppingCart shoppingCart;
    private String customerName;
    private String customerAddress;

    @BeforeEach
    void initVariable() {
        EShopController.startEShop();
        shoppingCart = EShopController.newCart();
        customerName = "Řehoř";
        customerAddress = "Pospíšilova";
    }

    @Test
    @Order(1)
    void testAddToCart_addItem_itemAdded() {
        Item item = new StandardItem(1, "Test Item", 100, "TEST", 10);
        shoppingCart.addItem(item);
        assertEquals(1, shoppingCart.getCartItems().size());
    }

    @Test
    @Order(2)
    void testRemoveFromCart_removeItem_itemRemoved() {
        Item item1 = new StandardItem(1, "Test Item 1", 100, "TEST", 10);
        Item item2 = new StandardItem(2, "Test Item 2", 200, "TEST", 5);

        shoppingCart.addItem(item1);
        shoppingCart.addItem(item2);

        shoppingCart.removeItem(item1.getID());

        assertEquals(1, shoppingCart.getCartItems().size());
    }

    @Test
    @Order(3)
    void testPurchaseShoppingCart_purchase_purchased() throws NoItemInStorage {
        Item item1 = new StandardItem(1, "Test Item 1", 100, "TEST", 10);
        Item item2 = new StandardItem(2, "Test Item 2", 200, "TEST", 5);
        Item item3 = new StandardItem(3, "Test Item 3", 300, "TEST", 0);

        shoppingCart.addItem(item1);
        shoppingCart.addItem(item2);
        shoppingCart.addItem(item3);

        assertEquals(3, shoppingCart.getCartItems().size());
    }

}