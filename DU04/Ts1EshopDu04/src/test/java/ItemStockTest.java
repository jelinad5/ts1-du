import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;
import shop.StandardItem;
import storage.ItemStock;

public class ItemStockTest {
//    TODO
//     konstruktor
//     parametrizovaný test pro metody změny počtu

     Item item;
     ItemStock itemStock;
     private int id = 66;
     private String name = "Item66";
     private float price = 42;
     private String category = "Category66";
     private int loyaltyPoints = 9;
     private int increasedItemCount = 0;

     @BeforeEach
     public void initVariable() {
          item = new StandardItem(id, name, price, category, loyaltyPoints);
          itemStock = new ItemStock(item);
     }

     @Test
     @Order(1)
     public void ItemStock_constructor_passed(){
          int returnedId = itemStock.getItem().getID();
          String returnedName = itemStock.getItem().getName();
          float returnedPrice = itemStock.getItem().getPrice();
          String returnedCategory = itemStock.getItem().getCategory();

          int returnedCount = itemStock.getCount();

          Assertions.assertEquals(id, returnedId);
          Assertions.assertEquals(name, returnedName);
          Assertions.assertEquals(price, returnedPrice);
          Assertions.assertEquals(category, returnedCategory);

          Assertions.assertEquals(increasedItemCount, returnedCount);
     }

     @ParameterizedTest
     @CsvSource({"0, 0", "1, 1", "-1, -1"})
     public void IncreaseItemCount_increaseCount_passed(int increase, int increaseResult) {
          increase += itemStock.getCount();
          increaseResult += itemStock.getCount();

          itemStock.IncreaseItemCount(increase);
          int returnedCount = itemStock.getCount();

          Assertions.assertEquals(returnedCount, increaseResult);
     }

     @ParameterizedTest
     @CsvSource({"0, 0", "1, 1", "-1, -1"})
     public void decreaseItemCount_decreasesCount_passed(int decrease, int decreaseResult){
          decrease += itemStock.getCount();
          decreaseResult += itemStock.getCount();

          itemStock.IncreaseItemCount(decrease);
          int returnedCount = itemStock.getCount();

          Assertions.assertEquals(returnedCount, decreaseResult);
     }

}