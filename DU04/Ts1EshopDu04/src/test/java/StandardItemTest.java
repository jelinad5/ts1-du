import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;

public class StandardItemTest {
//    TODO
//     konstruktor
//     copy
//     parametrizovaný test pro equals
    StandardItem standardItem;
    private int id = 66;
    private String name = "Item66";
    private float price = 42;
    private String category = "Category66";
    private int loyaltyPoints = 9;
    @BeforeEach
    public void initVariable(){
        standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
    }

    @Test
    @Order(1)
    public void StandardItem_constructor_passed(){
        int returnedId = standardItem.getID();
        String returnedName = standardItem.getName();
        float returnedPrice = standardItem.getPrice();
        String returnedCategory = standardItem.getCategory();
        int returnedLoyaltyPoints = standardItem.getLoyaltyPoints();

        Assertions.assertEquals(id, returnedId);
        Assertions.assertEquals(name, returnedName);
        Assertions.assertEquals(price, returnedPrice);
        Assertions.assertEquals(category, returnedCategory);
        Assertions.assertEquals(loyaltyPoints, returnedLoyaltyPoints);
    }

    @Test
    @Order(2)
    public void copy_copiesItem_passed(){
        StandardItem returnedStandardItem = standardItem.copy();

        int returnedId = returnedStandardItem.getID();
        String returnedName = returnedStandardItem.getName();
        float returnedPrice = returnedStandardItem.getPrice();
        String returnedCategory = returnedStandardItem.getCategory();
        int returnedLoyaltyPoints = returnedStandardItem.getLoyaltyPoints();

        Assertions.assertEquals(id, returnedId);
        Assertions.assertEquals(name, returnedName);
        Assertions.assertEquals(price, returnedPrice);
        Assertions.assertEquals(category, returnedCategory);
        Assertions.assertEquals(loyaltyPoints, returnedLoyaltyPoints);
    }

    @ParameterizedTest
    @CsvSource({
            "66, Item66, 42, Category66, 9, 66, Item66, 42, Category66, 9",
            "67, Item67, 43, Category67, 10, 67, Item67, 43, Category67, 10"
    })
    public void equals_compares_true(
            int id1, String name1, float price1, String category1, int loyaltyPoints1,
            int id2, String name2, float price2, String category2, int loyaltyPoints2
    ){
        StandardItem a = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem b = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);

        boolean answer = a.equals(b);
        Assertions.assertEquals(true, answer);
    }

}