import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.util.ArrayList;

public class OrderTest {
    @Test
    @org.junit.jupiter.api.Order(1)
    public void Order_nullConstructor_passed(){
        String customerName = null;
        String customerAddress = null;

        ArrayList<Item> item = new ArrayList<>();
        ShoppingCart shoppingCart = new ShoppingCart();

        Order order = new Order(shoppingCart, customerName, customerAddress);
        ArrayList<Item> itemsResult = order.getItems();
        String nameResult = order.getCustomerName();
        String addressResult = order.getCustomerAddress();
        int stateResult = order.getState();

        Assertions.assertEquals(item, itemsResult);
        Assertions.assertEquals(customerName, nameResult);
        Assertions.assertEquals(customerAddress, addressResult);
        Assertions.assertEquals(0, stateResult);
    }

    @Test
    @org.junit.jupiter.api.Order(2)
    public void Order_itemAndNullStateConstructor_passed(){
        int id = 66;
        String name = "Item66";
        float price = 42;
        String category = "Category66";
        int loyaltyPoints = 9;

        String customerName = "Řehoř";
        String customerAddress = "Pospíšilova";

        ArrayList<Item> item = new ArrayList<>();
        item.add(new StandardItem(id, name, price, category, loyaltyPoints));
        ShoppingCart shoppingCart = new ShoppingCart(item);

        Order order = new Order(shoppingCart, customerName, customerAddress);
        ArrayList<Item> itemsResult = order.getItems();
        String nameResult = order.getCustomerName();
        String addressResult = order.getCustomerAddress();
        int stateResult = order.getState();

        Assertions.assertEquals(item, itemsResult);
        Assertions.assertEquals(customerName, nameResult);
        Assertions.assertEquals(customerAddress, addressResult);
        Assertions.assertEquals(0, stateResult);
    }

    @Test
    @org.junit.jupiter.api.Order(3)
    public void Order_nullItemAndStateConstructor_passed(){
        String customerName = "Řehoř";
        String customerAddress = "Pospíšilova";

        ArrayList<Item> item = new ArrayList<>();
        ShoppingCart shoppingCart = new ShoppingCart();

        Order order = new Order(shoppingCart, customerName, customerAddress, 3);
        ArrayList<Item> itemsResult = order.getItems();
        String nameResult = order.getCustomerName();
        String addressResult = order.getCustomerAddress();
        int stateResult = order.getState();

        Assertions.assertEquals(item, itemsResult);
        Assertions.assertEquals(customerName, nameResult);
        Assertions.assertEquals(customerAddress, addressResult);
        Assertions.assertEquals(3, stateResult);
    }

    @Test
    @org.junit.jupiter.api.Order(4)
    public void Order_itemAndStateConstructor_passed(){
        int id = 66;
        String name = "Item66";
        float price = 42;
        String category = "Category66";
        int loyaltyPoints = 9;

        String customerName = "Řehoř";
        String customerAddress = "Pospíšilova";

        ArrayList<Item> item = new ArrayList<>();
        item.add(new StandardItem(id, name, price, category, loyaltyPoints));
        ShoppingCart shoppingCart = new ShoppingCart(item);

        Order order = new Order(shoppingCart, customerName, customerAddress, 3);
        ArrayList<Item> itemsResult = order.getItems();
        String nameResult = order.getCustomerName();
        String addressResult = order.getCustomerAddress();
        int stateResult = order.getState();

        Assertions.assertEquals(item, itemsResult);
        Assertions.assertEquals(customerName, nameResult);
        Assertions.assertEquals(customerAddress, addressResult);
        Assertions.assertEquals(3, stateResult);
    }

}
