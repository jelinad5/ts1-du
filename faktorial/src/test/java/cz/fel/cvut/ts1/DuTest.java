package cz.fel.cvut.ts1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static
        org.junit.jupiter.api.Assertions.*;

public class DuTest {
    @Test //anotace
    public void factorialTest() {
        Jelinad5 jelinad5 = new Jelinad5();
        int number = 3;
        long expectedResult = 6;

        long result = jelinad5.factorial(number);

        Assertions.assertEquals(expectedResult, result);
    }
}
